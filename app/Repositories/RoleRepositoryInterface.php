<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Role;

interface RoleRepositoryInterface
{
    public function all();

    public function findById($roleId);

    public function store(Request $request);

    public function update(Request $request, Role $role);

    public function destroy(Role $role);
   
    public function sortById();

    public function sortByName();

    public function searchByName($search);

}