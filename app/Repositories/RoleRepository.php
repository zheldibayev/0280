<?php

namespace App\Repositories;
use App\Models\Role;
use App\Http\Resources\RoleResource;
use Illuminate\Http\Request;


class RoleRepository implements RoleRepositoryInterface
{
    public function all()
    {
        return RoleResource::collection(Role::all());
    }

    public function findById($roleId)
    {
        return Role::where('id', $roleId)->with('users')->first();
    }

    public function store(Request $request)
    {
        $role = Role::create($request->all());
    }
    
    public function update(Request $request, Role $role)
    {
        $role->update($request->all());

        return new RoleResource($role);
    }

    public function destroy(Role $role)
    {
        $role->delete();
    }
    public function sortById()
    {
        return RoleResource::collection(Role::orderBy('id')->get());
    }

    public function sortByName()
    {
        return RoleResource::collection(Role::orderBy('name')->get());
    }

    public function searchByName($search)
    {
        return RoleResource::collection(Role::where('name', 'LIKE', "%{$search}%") ->get());
    }
}

