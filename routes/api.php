<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\RoleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/roles/sortById', [ RoleController::class, 'sortById']);

Route::get('/roles/sortByName', [ RoleController::class, 'sortByName']);

Route::get('/roles/searchByName/{search}', [ RoleController::class, 'searchByName']);

Route::apiResources([
    'roles' =>  RoleController::class,
    'users' => UserController::class,
]);
